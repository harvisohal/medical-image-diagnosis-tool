import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import models as models_t


from models.model import Model

import config


class Resnet(Model):

    def __init__(self, name):
        #Code to load from pretrained weights local file
        #model_ft = models_t.resnet18(pretrained=False)
        model_ft = torch.load('../weights/pretrained/resnetpretrained.pth')
        
        #print(model_ft)
        #model_ft = model_ft.load_state_dict(pretrained_weights)

        
        #Error begins here as for some reason the model_ft.fc is not a valid key
        num_ftrs = model_ft.fc.in_features
        # Here the size of each output sample is set to 2.
        # Alternatively, it can be generalized to nn.Linear(num_ftrs, len(class_names)).
        model_ft.fc = nn.Linear(num_ftrs, 2)

        model_ft = model_ft.to(config.device)

        self.final_conv_layer = model_ft.layer4

        criterion = nn.CrossEntropyLoss()

        # Observe that all parameters are being optimized
        optimizer = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)

        # Decay LR by a factor of 0.1 every 7 epochs
        scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)

        super().__init__(name, model_ft, optimizer, criterion, scheduler)
