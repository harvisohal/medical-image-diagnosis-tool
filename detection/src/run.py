import os
from PIL import Image

from torchvision import datasets,transforms, utils
import torch
import numpy as np
import matplotlib.pyplot as plt
import train

from config import MODEL, DATASET_PATH, WEIGHT_DIR, MODEL_DIR, MAX_EPOCHS
from dataset_loader import Dataset, imshow
from heatmap import ShowGradCamImage
from cropHeatmap import cropmap

def main(name, action, load_model=False, image_path=None):
    # Create the model object
    model = MODEL(name)
    if load_model:
        print("Trying to load model...")
        model.load_model(os.path.join(MODEL_DIR, name))
        #try - except block here
        print("Sucessfully loaded model")
    else:
        print("Sucessfully created model")


    # Perform the required action
    if(action == "train"):
        # Load the data
        print("\nTrying to load the dataset...")
        data = Dataset(DATASET_PATH)    
        inputs,classes = next(iter(data.data_loaders['train']))
        out = utils.make_grid(inputs)

        imshow(out,title = [data.class_names[x] for x in classes])
        print("Successfully loaded dataset\n")

        # train the model and show stats
        train.train_model(model, data, MAX_EPOCHS)
        model.save()
        model.plot_stat_history()
    elif(action == "test"):
        # Load the data
        print("\nTrying to load the dataset...")
        data = Dataset(DATASET_PATH)    
        print("Successfully loaded dataset\n")

        #model.plot_stat_history()
        model.plot_conf_matrix(data.data_loaders['val'])
        model.visualise_classifications(data.data_loaders['val'], class_type='FN')
    elif(action == "eval"):
        print("Loading image at path {}".format(image_path))
        with Image.open(image_path).convert('RGB') as image:
            output, probability = model.eval(image)
            plt.imshow(image)
            plt.show()
            print("Prediction:\t{}".format(output.item()))

            #libImpGradCAM.libImp(model.model, model.final_conv_layer, image, output)
            ShowGradCamImage.show_grad(image_path, os.path.join(MODEL_DIR, 'resnet_best'), 'resnet')
            if output.item() == 0:
                top,bottom,left,right = cropmap('heatmap.png', 'original_image.png') #added start to cropping based on heatmap
            else:
                print("No abnormality detected")

    else:
        print("Error: Unknown action")


if __name__ == "__main__":
    # To-Do: Use argparse to capture args 
    name = "resnet_best"
    action = "eval" # train, test, eval
    image_path = os.path.join(DATASET_PATH,'val','abnormal','MURA-v1.1validXR_WRISTpatient11205study1_positiveimage2.png') # optional input image for getting an output
    load_model = True

    main(name, action, load_model, image_path)
