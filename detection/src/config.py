"""Config file for fracture detection"""

import os
import torch
import models.resnet , models.vgg , models.densenet

MAX_EPOCHS = 15
NUM_WORKERS = 1
BATCH_SIZE = 8

INTERACTIVE_MODE = False # Set to True when using jupyter notebook

MODEL = models.resnet.Resnet # model class to train
#MODEL = models.densenet.Densenet # model class to train
#MODEL = models.vgg.VGG # model class to train

#TARGET_LAYER = models.resnet.Resnet.model.layer4
#TARGET_LAYER = models.vgg.VGG[0].features[29]

DATASET = 'mura'
DATASET_PATH = os.path.join(os.path.dirname(os.getcwd()),'dataset', 'MURA')
WEIGHT_DIR = os.path.join(os.path.dirname(os.getcwd()),'weights','pretrained')
MODEL_DIR = os.path.join(os.path.dirname(os.getcwd()), 'models') 
MEAN = [0.2726, 0.2726, 0.2726]
STD = [0.1801, 0.1801, 0.1801]

#DATASET = 'shaip-classification'
#DATASET_PATH = os.path.abspath('/mnt/shaip/workspace/gt/classification')
#WEIGHT_DIR = os.path.join(os.path.dirname(os.getcwd()),'weights','pretrained')
#MODEL_DIR = os.path.abspath('/mnt/shaip/workspace/cache/uol_project/models') 
#MEAN = [0.3842, 0.3842, 0.3842]
#STD = [0.2154, 0.2154, 0.2154]

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
