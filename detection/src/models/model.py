import torch
from torch.autograd import Variable
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

import config
import dataset_loader
from heatmap import ShowGradCamImage

class Model:
    """Encapsulates the pytorch model and relevant other information, can be saved and loaded in full"""

    def __init__(self, name, model, optimizer, criterion = None, scheduler = None):
        self.name = name
        self.model = model
        self.optimizer = optimizer
        self.criterion = criterion
        self.scheduler = scheduler
        self.epoch = 0
        self.statsrec = [['train_loss'],['val_loss'],['val_acc']]
        self.accuracy = 0.0


    def save(self, best=False):
        '''Save the model and its related information'''
        try:
            checkpoint = {
                'name': self.name,
                'epoch': self.epoch,
                'statsrec': self.statsrec,
                'model_state_dict': self.model.state_dict(),
                'optimizer_state_dict': self.optimizer.state_dict()
            }
            if self.scheduler:
                checkpoint.update({'scheduler_state_dict': self.scheduler.state_dict()})

            if best:
                model_save_name = "_".join([self.name, 'best'])
            else:
                model_save_name = "{}_{}_{:.4f}".format(self.name, self.epoch, self.accuracy)
            model_path = os.path.join(config.MODEL_DIR, model_save_name)
            torch.save(checkpoint, model_path)
            print("Saved model to {}".format(model_path))
        except Exception as err:
            print("ERROR: failed to save model due to {}".format(err))


    def load_model(self, model_name):
        '''Load a model at the given filepath, uses the current MODEL object defined in config.py'''
        try:
            model_path = model_path = os.path.join(config.MODEL_DIR, model_name)
            checkpoint = torch.load(model_path)
            self.model.load_state_dict(checkpoint['model_state_dict'])
            self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            self.name = checkpoint['name']
            self.epoch = checkpoint['epoch']
            self.statsrec = checkpoint['statsrec']

            if self.scheduler:
                self.scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
            print("Loaded model from {}".format(model_path))
        except Exception as err:
            print("ERROR: failed to load model due to, {}".format(err))
            sys.exit(0)


    def eval(self, image):
        '''Get an output for the given input image, must be in PIL format'''
        self.model.eval()
        image_tensor = dataset_loader.data_transforms['val'](image)
        image_tensor = Variable((image_tensor.unsqueeze(0)))
        output = self.model(image_tensor)
        i, pred = torch.max(output, 1)
        return pred, i


    def export_weights(self, filepath):
        '''Export model weights only to the given filepath'''
        pass


    def plot_stat_history(self):
        '''Saves a png image of the loss and evaluation metrics of the model 
        over epochs'''
        print("\nCreating stats history graph in statsrec.png...")
        fig, ax1 = plt.subplots()
        plt.plot(self.statsrec[0][1:], 'r', label = 'training loss')
        plt.plot(self.statsrec[1][1:], 'g', label = 'validation loss')
        plt.legend(loc='center')
        plt.xlabel('epoch')
        plt.ylabel('loss')
        plt.title('Training and validation loss, and validation accuracy')
        ax2=ax1.twinx()
        ax2.plot(self.statsrec[2][1:], 'b', label = 'validation accuracy')
        ax2.set_ylabel('accuracy')
        plt.legend(loc='upper left')
        plt.savefig('statsrec.png')
        print("Saved model statistics plot.")


    def plot_conf_matrix(self, data_loader):
        '''Saves a png image of the confusion matrix for the given model
        on the specified dataset'''
        print("\nCreating confusion matrix in confmatrix.png...")
        class_num = 2
        conf_matrix = np.zeros((class_num, class_num), np.int8)
        
        with torch.no_grad():
            for data in data_loader: # iterative for small memory systems
                images, labels = data
                outputs = self.model(images)
                _, predicted = torch.max(outputs.data, 1)
                
                conf_matrix = np.add(conf_matrix, confusion_matrix(labels, predicted, labels=[0, 1]))
                
        cm_display = ConfusionMatrixDisplay(confusion_matrix=conf_matrix, display_labels=['abnormal','normal'])
        cm_display.plot(xticks_rotation='vertical')
        cm_display.ax_.set_title("Confusion matrix for {}".format(self.name))
        plt.savefig('confmatrix.png')
        print("Saved confusion matrix")

        acc, sens, spec = self.get_heuristics(conf_matrix)
        print("Accuracy: {}\nSensitivity: {}\nSpecificity: {}".format(acc, sens, spec))


    def get_heuristics(self, confusion_matrix):
        '''Get the accuracy, sensitivity and specificity of the given confusion matrix'''
        TP = confusion_matrix[0][0]
        FN = confusion_matrix[0][1]
        FP = confusion_matrix[1][0]
        TN = confusion_matrix[1][1]
        accuracy = (TP+TN)/(TP+TN+FP+FN)
        sensitivity = TP/(TP+FN)
        specificity = TN/(TN+FP)
        return (accuracy, sensitivity, specificity)


    def visualise_classifications(self, data_loader, class_type='ALL', amount=5):
        self.model.eval()
        with torch.no_grad():
            for data in data_loader:
                #get output
                images, labels = data
                for i in range(0,len(labels)):
                    image_tensor = Variable((images[i].unsqueeze(0)))
                    output = self.model(image_tensor)
                    prob, pred = torch.max(output, 1)

                    # visualise if correct class type
                    if(pred == 0 and labels[i] == 0 and (class_type=='TP' or class_type=='ALL')):
                        print("\nTP with probability: {}".format(prob))
                        dataset_loader.imshow(images[i])
                        ShowGradCamImage.show_grad(image_path, os.path.join(MODEL_DIR, 'resnet_best'), 'resnet')
                    elif(pred == 0 and labels[i] == 1 and (class_type=='FP' or class_type=='ALL')):
                        print("\nFP with probability: {}".format(prob))
                        dataset_loader.imshow(images[i])
                    elif(pred == 1 and labels[i] == 0 and (class_type=='FN' or class_type=='ALL')):
                        print("\nFN with probability: {}".format(prob))
                        dataset_loader.imshow(images[i])
                    elif(pred == 1 and labels[i] == 1 and (class_type=='TN' or class_type=='ALL')):
                        print("\nTN with probability: {}".format(prob))
                        dataset_loader.imshow(images[i])
