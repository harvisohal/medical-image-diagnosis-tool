jupyter notebook \
 --port=8888 \
 --NotebookApp.token='' \
 --no-browser \
 --ip=0.0.0.0 \
 --NotebookApp.allow_origin='*'
