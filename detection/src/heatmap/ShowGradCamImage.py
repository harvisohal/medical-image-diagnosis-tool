## TODO: Change this to ipynb file

from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
from torchvision import transforms
import skimage.transform

from heatmap import GradCam

## TODO: This import has to be checked!
from models import resnet

#IMAGE_PATH = "/content/drive/MyDrive/Masters Project/image.png"
#MODEL_PRETRAINED_PATH = "/content/drive/MyDrive/Masters Project/resnet_best.zip"
#MODEL_NAME = "Test1"

def show_grad(IMAGE_PATH, MODEL_PRETRAINED_PATH, MODEL_NAME):
  image = Image.open(IMAGE_PATH).convert('RGB')
  model = resnet.Resnet(MODEL_NAME)
  model.load_model(MODEL_PRETRAINED_PATH)
  model = model.model

  ## TODO: Find a better way of getting the final layer
  final_layer = model.layer4

  tensor = GradCam.GradCAM.preProcessImage(image)
  activated_features, weight_softmax, class_idx = GradCam.GradCAM.prepareGradCAM(tensor, model, final_layer)
  overlay = GradCam.GradCAM.getCAM(activated_features.features, weight_softmax, class_idx )

  display_transform = transforms.Compose([
    transforms.Resize((250,250))
  ])

  fig = plt.figure(frameon=False)
  ax = plt.Axes(fig, [0., 0., 1., 1.])
  ax.set_axis_off()
  fig.add_axes(ax)
  ax.imshow(display_transform(image), aspect='auto')
  ax.imshow(skimage.transform.resize(overlay[0], tensor.shape[1:3]), alpha=0.0, cmap="tab20b")
  fig.savefig("original_image.png", bbox_inches="tight", pad_inches=0)
  ax.imshow(skimage.transform.resize(overlay[0], tensor.shape[1:3]), alpha=0.7,cmap="tab20b")
  fig.savefig("slight-overlay.png", bbox_inches="tight", pad_inches=0)
  ax.imshow(skimage.transform.resize(overlay[0], tensor.shape[1:3]),cmap="tab20b")
  fig.savefig("heatmap.png", bbox_inches="tight", pad_inches=0)




  #fig.show()

  imshow(display_transform(image))
  imshow(skimage.transform.resize(overlay[0], tensor.shape[1:3]), alpha=0.7, cmap='jet')

  #plt.savefig('output1.png')
  plt.show()
  """
  
  """
