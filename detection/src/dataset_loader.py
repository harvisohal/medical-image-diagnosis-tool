"""
Class to create training,validation and test data loaders.
Declaration of data transforms.
Uses DATASET PATH from config.py."""

import torch
from torchvision import datasets,transforms, utils
import numpy as np
import matplotlib.pyplot as plt
import os

from config import BATCH_SIZE, NUM_WORKERS, INTERACTIVE_MODE, DATASET, MEAN, STD

if INTERACTIVE_MODE:
    plt.ion()

data_transforms = {
            'train': transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(256),
                transforms.RandomHorizontalFlip(),
                transforms.RandomVerticalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(MEAN, STD)
                ]),
            'val': transforms.Compose([

                transforms.Resize(256),
                #transforms.CenterCrop(256),
                transforms.ToTensor(),
                transforms.Normalize(MEAN, STD)
                ]),
            'test': transforms.Compose([

                transforms.Resize(256),
                #transforms.CenterCrop(256),
                transforms.ToTensor(),
                ]),
            }

def imshow(inp, title = None):
        """Imshow for Tensor"""
        inp = inp.numpy().transpose((1,2,0))
        mean = np.array(MEAN)
        std = np.array(STD)
        inp = std*inp+mean
        inp = np.clip(inp,0,1)
        plt.imshow(inp)
        if title is not None:
            plt.title(title)
        plt.pause(0.001)
        plt.show()
        
class Dataset:
    """Represent a dataset and store objects to access the underlying data"""

    def __init__(self, data_dir):
        if (DATASET == "mura") or (DATASET == "practice"):
            self.load_mura(data_dir)
        elif DATASET == "shaip-classification":
            self.load_shaip_classification(data_dir)


    def load_mura(self, data_dir):
        """Load the MURA dataset, must be preprocessed using format_mura.py"""
        self.image_datasets = {x:datasets.ImageFolder(os.path.join(data_dir,x),data_transforms[x]) for x in ['train','val']}
        self.data_loaders = {x:torch.utils.data.DataLoader(
                self.image_datasets[x],batch_size = BATCH_SIZE,shuffle = True, num_workers=NUM_WORKERS)for x in ['train','val']}
        self.dataset_sizes = {x:len(self.image_datasets[x]) for x in ['train','val']}
        self.class_names = self.image_datasets['train'].classes


    def load_shaip_classification(self, data_dir):
        """Load the SHAIP classification dataset"""
        self.image_datasets = {x:datasets.ImageFolder(os.path.join(data_dir,x,'filtered'),data_transforms[x]) for x in ['train','val']}
        self.data_loaders = {x:torch.utils.data.DataLoader(
                self.image_datasets[x],batch_size = BATCH_SIZE,shuffle = True, num_workers=NUM_WORKERS)for x in ['train','val']}
        self.dataset_sizes = {x:len(self.image_datasets[x]) for x in ['train','val']}
        self.class_names = self.image_datasets['train'].classes

    

